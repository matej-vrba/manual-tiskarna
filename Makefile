##
# Project Title
#
# @file
# @version 0.1

src=$(wildcard *.tex)

.PHONY: all clean

all: main.pdf

main.pdf: $(src)
	latexmk -pdf -xelatex main.tex

clean:
	rm -f *.aux *.bcf *.log *.out  *.run.xml *.rubbercache *.bbl *.blg *.toc *.dvi *.fdb_latexmk *.fls *.pdf *.xdv

pack: manual.tar

manual.pdf: main.pdf
	cp $^ $@

manual.tar: manual.pdf unix/Ricoh-Aficio_MP_C2800_PDF.ppd windows/r01294L1a.exe
	pax -w  $^ > $@
